import { Component, Input, Inject ,OnInit } from '@angular/core';
import {AppareilService} from '../services/appareil.service';
@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {
  @Input() AppareilName?: string;
  @Input() AppareilStatus?: string;
  @Input() IndexOfAppareil?: number;
  @Input() id?:number;

 constructor( private AppareilService: AppareilService) {}

  ngOnInit(): void {
  }
      getStatus(){
      return this.AppareilStatus;
      }
      getColor(){
          if(this.AppareilStatus === 'allumé'){
          return 'green';
          } else if(this.AppareilStatus === 'éteint'){
          return 'red';
          }
      }
      onSwitchOn(){
        this.AppareilService.switchOnOne(this.IndexOfAppareil!);
      }
      onSwitchOff(){
        this.AppareilService.switchOffOne(this.IndexOfAppareil!);
      }
}
