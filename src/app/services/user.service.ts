import { Subject } from "rxjs";
import { User } from "../models/User.model";

export class UserService{
    private users:User[]=[
        {
            firstname:'James',
            lastName:'Smith',
            email:'Jamessmith@gmail.com',
            drinkPreference:'Coca',
            hobbies:[
                'Coder',
                'La dégustation de café'
            ]
        }
    ];
    UserSubject=new Subject<User[]>();

    emitUsers(){
        this.UserSubject.next(this.users.slice());
    }

    addUsers(user:User){
        this.users.push(user);
        this.emitUsers();
    }
}