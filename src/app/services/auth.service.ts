export class AuthService {
    isAuth=false;

    signIn(){
        return new Promise(
           (solve,reject) => {
            setTimeout(()=>{
                this.isAuth = true;
                solve(true);
            }, 4000);
            }
        );      
    }

    signOut(){
        this.isAuth=false;
    }

}
