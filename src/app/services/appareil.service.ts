import { Subject } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root',
})

export class AppareilService{

  appareilSubject = new Subject;
  private appareils=[
  { 
    id:1,
    name:'Machine à laver',
    status:'éteint'
  },
  {
    id:2,
    name:'Télévision',
    status:'éteint'
  },
  {   
     id:3,
     name:'Ordinateur',
     status:'allumé'
  }
  ];
  constructor(private httpClient:HttpClient){}

    emitAppareilSubject(){
      this.appareilSubject.next(this.appareils.slice())
    }
  getAppareilById(id:number){
    const appareil=this.appareils.find((appareilObject)=>{
        return appareilObject.id===id;
    }
    );
    return appareil;
  }
  switchOnAll(){
    for (let appareil of this.appareils){
      appareil.status='allumé'
    }
    this.emitAppareilSubject();
  }

  switchOffAll(){
      for (let appareil of this.appareils){
        appareil.status='éteint'
      }
      this.emitAppareilSubject();
    }

    switchOnOne(index:number){
          this.appareils[index].status='allumé'
          this.emitAppareilSubject();
        }
    switchOffOne(index:number){
              this.appareils[index].status='éteint'
              this.emitAppareilSubject();
            }

    addAppareil(name:string,status:string){
      const appareilObject={
        id:0,
        name:'',
        status:''
      };
      appareilObject.name=name;
      appareilObject.status=status;
      appareilObject.id=this.appareils[(this.appareils.length-1)].id+1;
      this.appareils.push(appareilObject);
      this.emitAppareilSubject();
    }

    saveAppareilsToServer(){
      this.httpClient
      .put('https://http-client-demo-lordwin-default-rtdb.europe-west1.firebasedatabase.app/appareils.json',this.appareils)
      .subscribe(
        ()=> {
          console.log('Enregistrement terminé');
        },
        (error)=>{
          console.log('Erreur de sauvegarde ! '+error);
        }
      )
    }

    getAppareilFromServer(){
      this.httpClient
      .get<any>('https://http-client-demo-lordwin-default-rtdb.europe-west1.firebasedatabase.app/appareils.json')
      .subscribe(
        (Response)=>{
          this.appareils=Response;
          this.emitAppareilSubject();
        },
        (error)=>{
          console.log('Erreur de Chargement ! '+error);
        }
      )
    }
}
