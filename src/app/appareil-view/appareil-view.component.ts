import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {AppareilService} from '../services/appareil.service';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {

  IsAuth=false;
   lastUpdate = new Date();

  /* lastUpdate= new Promise(
       (resolve, reject) => {
         const date = new Date();
          setTimeout(()=>{
                 resolve(date);
             }, 2000);
       }
   )
   */

   appareils?:any [];
   appareilsubscription?:Subscription;
   constructor( private AppareilService:AppareilService) {
         setTimeout(()=>{
         this.IsAuth = true;
     }, 4000);
   }
   ngOnInit(): void{
     this.appareilsubscription=this.AppareilService.appareilSubject.subscribe(
       (appareils:any) => {
         this.appareils=appareils
       }
     );
     this.AppareilService.emitAppareilSubject();

   }
   onAllumer(){
     this.AppareilService.switchOnAll();
   }
   onEteindre(){
     this.AppareilService.switchOffAll();
     }

    onSave(){
      this.AppareilService.saveAppareilsToServer();
    }

    onFetch(){
      this.AppareilService.getAppareilFromServer();
    }
}
