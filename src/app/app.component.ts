import { Component ,OnDestroy,OnInit } from '@angular/core';
import {AppareilService} from './services/appareil.service';
import { Observable, Subscription} from 'rxjs';
import { interval } from 'rxjs';
import 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit ,OnDestroy{

      secondes?:number;
      counterSubscription?:Subscription;

  constructor(){}

  ngOnInit(): void {
    const source = interval(1000);
    this.counterSubscription=source.subscribe(
      (value:number)=>{
         this.secondes=value;
      }
    );
  }


  ngOnDestroy(): void {
      this.counterSubscription?.unsubscribe();
  }


}
