export class User{


    constructor(public firstname:string,
        public lastName:string,
        public email:string,
        public drinkPreference:string,
        public hobbies:string []){

    }
}