import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/User.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

  userForm!:FormGroup;

  constructor( private formBuilder: FormBuilder,
                private userservice: UserService,
                private router:Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.userForm=this.formBuilder.group(
      {
        firstname:['', Validators.required],
        lastName:['', Validators.required],
        email:['', [Validators.required, Validators.email]],
        drinkPreference:['', Validators.required],
        hobbies:this.formBuilder.array([])
      }
    );
  }
  onSubmitForm(){
    const formValue=this.userForm.value;
    const newUser=new User(
      formValue['firstname'],
      formValue['lastName'],
      formValue['email'],
      formValue['drinkPreference'],
      formValue['hobbies']?formValue['hobbies']:[]
    );
    this.userservice.addUsers(newUser);
    this.router.navigate(['/users']);
  }

  getHobbies(){
    return this.userForm.get('hobbies') as FormArray;
  }
  onAddHobby(){
    const newhobbyControl=this.formBuilder.control('', Validators.required);
    this.getHobbies().push(newhobbyControl);
  }

}